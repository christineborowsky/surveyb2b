jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
		"sap/ui/test/Opa5",
		"eon/app/com/survey/test/integration/pages/Common",
		"sap/ui/test/opaQunit",
		"eon/app/com/survey/test/integration/pages/Worklist",
		"eon/app/com/survey/test/integration/pages/Object",
		"eon/app/com/survey/test/integration/pages/NotFound",
		"eon/app/com/survey/test/integration/pages/Browser",
		"eon/app/com/survey/test/integration/pages/App"
	], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "eon.app.com.survey.view."
	});

	sap.ui.require([
		"eon/app/com/survey/test/integration/WorklistJourney",
		"eon/app/com/survey/test/integration/ObjectJourney",
		"eon/app/com/survey/test/integration/NavigationJourney",
		"eon/app/com/survey/test/integration/NotFoundJourney",
		"eon/app/com/survey/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});