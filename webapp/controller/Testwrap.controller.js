sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("eon.app.com.survey.controller.Testwrap", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf eon.app.com.survey.view.Testwrap
		 */
		onInit: function() {
				var jsonModel = new sap.ui.model.json.JSONModel(
				{
					LeadType: "NATUR",
					LeadChannel:"MOB",
					AnschlussGasJa:false,
					AnschlussGasNein:false,
					VersorgungsgebietJa:false,
					VersorgungsgebietNein:false,
					WarmwasserJa:false,
					WarmwasserNein:false,
					PufferJa:false,
					PufferNein:false,
					BHKWGebaeudeJa:false,
					BHKWGebaeudeNein:false,
					BHKWIndustrieJa:false,
					BHKWIndustrieNein:false,
					EinsatzStromJa:false,
					EinsatzStromNein:false,
					EinsatzGasJa:false,
					EinsatzGasNein:false,
					EinsatzFlGasJa:false,
					EinsatzFlGasNein:false,
					EinsatzOelJa:false,
					EinsatzOelNein:false,
					EinsatzBioJa:false,
					EinsatzBioNein:false,
					WaermekundeJa:false,
					WaermekundeNein:false
				});
			        
			this.getView().setModel(jsonModel, "input");
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf eon.app.com.survey.view.Testwrap
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf eon.app.com.survey.view.Testwrap
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf eon.app.com.survey.view.Testwrap
		 */
		//	onExit: function() {
		//
		//	}

	});

});