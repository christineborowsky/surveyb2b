sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("eon.app.com.survey.controller.Commodity", {

		onInit: function() {
		
			var model = new sap.ui.model.json.JSONModel();
			model.setData( {
					offerData: [{
						Text: "Vertragsverlängerung",
						Selected: false
					}, {
						Text: "Preisanpassung",
						Selected: false
					}, {
						Text: "neuer Vertrag",
						Selected: false
					}]
				}
			);
			this.getView().setModel(model, "valueHelp"); 

		},
	
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			var sGuid = oEvent.getParameter("arguments").Guid;
			if (sGuid !== undefined) {
				this.getModel().metadataLoaded().then(function() {
					var sObjectPath = this.getModel().createKey("LeadSet", {
						Guid: sGuid
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
			}
		},
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function(sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("viewModel");
			
			
			
			this.mergeInputModel( this.getModel().getProperty(sObjectPath) );
			
			
			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);
			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function() {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},
		_onBindingChange: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
			//	this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				//this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
			var sPath = oElementBinding.getPath();
			var	oResourceBundle = this.getResourceBundle();
			var	oObject = oView.getModel().getObject(sPath);
			 
			var	sGuid = oObject.Guid;
			var	oViewModel = this.getModel("viewModel");
			
			 
		/*	oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("shareSaveTileAppTitle", ["Lead"]));
			oViewModel.setProperty("/shareOnJamTitle", sGuid);
			oViewModel.setProperty("/shareSendEmailSubject", oResourceBundle.getText("shareSendEmailObjectSubject", [sGuid]));
			oViewModel.setProperty("/shareSendEmailMessage", oResourceBundle.getText("shareSendEmailObjectMessage", [
				sGuid,
				location.href
			]));*/
		

		 
		}

	});

});