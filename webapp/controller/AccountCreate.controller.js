sap.ui.define([
	"eon/app/com/survey/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("eon.app.com.survey.controller.AccountCreate", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf eon.app.com.survey.view.AccountCreate
		 */
		onInit: function () {
			debugger;
			var viewModel = new JSONModel({
				"busy": "false"
			});
			this.getView().setModel(viewModel, "viewModel");

			this.getView().setModel(new JSONModel(), "input");
			this.setModel(this.getOwnerComponent().getModel());
			/*	var sObjectPath = this.getModel().createKey("DubSearchSet", {
					BuPartner: '324234'
				});
				this._bindView("/" + sObjectPath);*/
			//	this._bindView(sObjectPath);
		},
		savePartner: function (oEvent) {
			var oModel = this.getModel("input");
			/*		var oEntry = {};
					oEntry.HouseNum = oModel.getProperty("HouseNum");
			Object.keys(oModel.getData()).forEach(function(element)
			{
				console.log(oModel.getProperty("/"+element));
				
			});*/
			this.getModel("viewModel").setProperty("/busy", true);
			this.getModel().create("/DubSearchSet", oModel.getData(), {
				method: "POST",
				success: function (data) {
					this.getModel("viewModel").setProperty("/busy", false);
					debugger;
				},
				error: function (response) {
					debugger;
					this.getModel("viewModel").setProperty("/busy", false);
				}
			});

		},
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function (sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("viewModel");

			//this.mergeInputModel(this.getModel().getProperty(sObjectPath));

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);
			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function () {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},
		_onBindingChange: function () {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				//	this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				//this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
			var sPath = oElementBinding.getPath();
			var oResourceBundle = this.getResourceBundle();
			var oObject = oView.getModel().getObject(sPath);

			//	var sGuid = oObject.Guid;
			//	var oViewModel = this.getModel("viewModel");

			/*	oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("shareSaveTileAppTitle", ["Lead"]));
				oViewModel.setProperty("/shareOnJamTitle", sGuid);
				oViewModel.setProperty("/shareSendEmailSubject", oResourceBundle.getText("shareSendEmailObjectSubject", [sGuid]));
				oViewModel.setProperty("/shareSendEmailMessage", oResourceBundle.getText("shareSendEmailObjectMessage", [
					sGuid,
					location.href
				]));*/

		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf eon.app.com.survey.view.AccountCreate
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf eon.app.com.survey.view.AccountCreate
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf eon.app.com.survey.view.AccountCreate
		 */
		//	onExit: function() {
		//
		//	}

	});

});