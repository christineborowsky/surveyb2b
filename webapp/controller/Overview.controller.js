sap.ui.define([
	"eon/app/com/survey/controller/BaseController",
	"jquery.sap.global",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/model/Filter"
], function(BaseController, jQuery, Controller, JSONModel, MessageToast, Filter) {
	"use strict";
	return BaseController.extend("eon.app.com.survey.controller.Overview", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf eon.app.com.survey.view.view.Overview
		 */
		onInit: function(evt) {
			// set mock model
			//var sPath = jQuery.sap.getModulePath("eon.app.com.survey.model", "/tiles.json");
			//var oModel = new JSONModel(sPath);
			//this.getView().setModel(oModel);
			var viewModel = new JSONModel({
				"pics": jQuery.sap.getModulePath("eon.app.com.survey.media", "")
			});
			this.getView().setModel(viewModel, "viewModel");
			this.getRouter().getRoute();
			this.getView().byId("openLeads").addEventDelegate({
				onsapdown: function() {}
			});
		},
		handleEditPress: function(evt) {
			var oTileContainer = this.getView().byId("container");
			var newValue = !oTileContainer.getEditable();
			oTileContainer.setEditable(newValue);
			evt.getSource().setText(newValue ? "Done" : "Edit");
		},
		handleTilePress: function(evt) {
			var id = evt.getSource().getId();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			if (id.indexOf("pv") !== -1) {
				oRouter.navTo("pvCreate");
			}
			if (id.indexOf("natur") !== -1) {
				oRouter.navTo("naturCreate");
			}
			if (id.indexOf("commodity") !== -1) {
				oRouter.navTo("commodityCreate");
			}
			if (id.indexOf("emob") !== -1) {
				MessageToast.show("LeaderfassungUI in Arbeit. Coming soon!"); //oRouter.navTo("emob");
			} //this.getRouter().navTo("survey");
			//	this.getRouter().navTo("survey");
		},
		navView: function(evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("testwrapCreate");
			},
		handleBusyPress: function(evt) {
			var oTileContainer = this.getView().byId("container");
			var newValue = !oTileContainer.getBusy();
			oTileContainer.setBusy(newValue);
			evt.getSource().setText(newValue ? "Done" : "Busy state");
		},
		handleTileDelete: function(evt) {
			var tile = evt.getParameter("tile");
			evt.getSource().removeTile(tile);
		},
		formatStringToFloat: function(oValue) {
			return parseFloat(oValue);
		},
		onLeadsRequested: function(oEvent) {
			var oModel = this.getView().getModel();
			oModel.read("/CustomizingLeadChannelSet", {
				success: $.proxy(function(oRetrievedResult) {
					if (oRetrievedResult.results.length > 0) {
						this.oLeadChannels = oRetrievedResult.results;
					}
				}, this),
				error: function(oError) {
					MessageToast.show("Datenbank nicht erreicht");
				}
			});
			oModel.read("/CustomizingLeadStatusSet", {
				success: $.proxy(function(oRetrievedResult) {
					if (oRetrievedResult.results.length > 0) {
						this.oLeadStatus = oRetrievedResult.results;
					}
				}, this),
				error: function(oError) {
					MessageToast.show("Datenbank nicht erreicht");
				}
			});
		},
		formatLeadChannel: function(oValue) {
			var oEntry = this.oLeadChannels.filter(function(data) {
				return data.LeadChannel === oValue;
			});
			return oEntry[0].LeadChannelDescription;
		},
		formatStatus: function(oValue) {
			var oEntry = this.oLeadStatus.filter(function(data) {
				return data.StatusCode === oValue;
			});
			return oEntry[0].StatusDescription;
		},
		filterTable: function(oEvent) {
			var oFilter = [
				new sap.ui.model.Filter("LeadChannel", "EQ", this.getView().byId("slChannel").getSelectedKey()),
				new sap.ui.model.Filter("Status", "EQ", this.getView().byId("slStatus").getSelectedKey())
			];
			this.byId("openLeads").getBinding("items").filter(oFilter);
		},
		handleListItemPress: function(evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var selectedLead = evt.getSource().getBindingContext().getProperty("Guid");
			var selectedType = evt.getSource().getBindingContext().getProperty("LeadType").toLowerCase();
				if (selectedType.indexOf("pv") !== -1) {
				oRouter.navTo("pv", {
				Guid: selectedLead
			});
			}
			if (selectedType.indexOf("natur") !== -1) {
				oRouter.navTo("natur", {
				Guid: selectedLead
			});
				 
			}
			
		
		}
	
	});
});