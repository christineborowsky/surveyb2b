sap.ui.define([
		"eon/app/com/survey/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("eon.app.com.survey.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);