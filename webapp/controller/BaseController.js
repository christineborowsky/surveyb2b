sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";

	return Controller.extend("eon.app.com.survey.controller.BaseController", {
		/**
		 * Convenience method for accessing the router.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @public
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Event handler when the share by E-Mail button has been clicked
		 * @public
		 */
		onShareEmailPress: function () {
			var oViewModel = (this.getModel("objectView") || this.getModel("worklistView"));
			sap.m.URLHelper.triggerEmail(
				null,
				oViewModel.getProperty("/shareSendEmailSubject"),
				oViewModel.getProperty("/shareSendEmailMessage")
			);
		},
		saveLead: function (oModel) {
			//var oModel = this.getModel( "input");

			var jsonString = oModel.getJSON();

			var oEntry = {};

			oEntry.LeadType = this.getModel("input").getProperty("/LeadType");
			//WENN DAS LEER DANN EINFACH LEEREN STRING REIN: 
			if (this.getModel("input").getProperty("/GPNum") !== undefined) {
				oEntry.Partner = this.getModel("input").getProperty("/GPNum");
			} else {
				MessageToast.show("Keine ausreichende Kundendaten");
				return;
				//oEntry.Partner = "";
			}
			if (this.getModel("input").getProperty("/Erfolg") !== undefined) {
				oEntry.Rating = this.getModel("input").getProperty("/Erfolg").toString();
			} else {
				oEntry.Rating = "";
			}
			if (this.getModel("input").getProperty("/LeadChannel") === undefined) {
				oEntry.LeadChannel = "MOB";
			}
			if (this.getModel("input").getProperty("/Status") === undefined) {
				oEntry.Status = "INPROGRESS";
			}
			oEntry.LeadXml = jsonString;
			this.getView().byId("CreateProductWizard").setBusy(true);

			if (this.getView().getBindingContext() === undefined) {
				oEntry.Guid = "";

				this.getModel().create("/LeadSet", oEntry, {
					success: $.proxy(function (oRetrievedResult) {
						this.getView().byId("CreateProductWizard").setBusy(false);
						MessageToast.show("Lead " + oRetrievedResult.LeadId + " gespeichert");
					}, this),
					error: $.proxy(function (oError) {
						this.getView().byId("CreateProductWizard").setBusy(false);
					}, this)
				});
			} else {
				this.getModel().update(this.getView().getBindingContext().getPath(), oEntry, {
					success: $.proxy(function (oRetrievedResult) {
						this.getView().byId("CreateProductWizard").setBusy(false);
						MessageToast.show("Lead gespeichert");
					}, this),
					error: $.proxy(function (oError) {
						this.getView().byId("CreateProductWizard").setBusy(false);
					}, this)
				});

			}
		},
		handleSelectDialogPress: function (oEvent) {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("eon.app.com.survey.fragments.Dialog", this);
				this._oDialog.setModel(this.getView().getModel());
			}

			var oFilters = [];
			var oModel = this.getModel("input");

			if (oModel.getProperty("/Kundenname") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("NameOrg1", "EQ", oModel.getProperty("/Kundenname")));
			}
			if (oModel.getProperty("/Strasse") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("Street", "EQ", oModel.getProperty("/Strasse")));
			}
			if (oModel.getProperty("/PLZ") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("PostCode", "EQ", oModel.getProperty("/PLZ")));
			}
			if (oModel.getProperty("/Hausnummer") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("HouseNum", "EQ", oModel.getProperty("/Hausnummer")));
			}
			if (oModel.getProperty("/Ort") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("City", "EQ", oModel.getProperty("/Ort")));
			}
			if (oModel.getProperty("/GPNum") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("BuPartner", "EQ", oModel.getProperty("/GPNum")));
			}
			if (oModel.getProperty("/VKNum") !== undefined) {
				oFilters.push(new sap.ui.model.Filter("BuagID", "EQ", oModel.getProperty("/VKNum")));
			}
			this._oDialog.getBinding("items").filter(oFilters);
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
		},
		handleClose: function (oEvent) {
			if (oEvent.getId() === "cancel") {
				return;
			}
			var oModel = this.getModel("input");
			if ("selectedItem" !== undefined) {

				oModel.setProperty("/Hausnummer", oEvent.getParameter("selectedItem").getBindingContext().getProperty("HouseNum"));
				oModel.setProperty("/Strasse", oEvent.getParameter("selectedItem").getBindingContext().getProperty("Street"));
				oModel.setProperty("/Kundenname", oEvent.getParameter("selectedItem").getBindingContext().getProperty("NameOrg1"));
				oModel.setProperty("/PLZ", oEvent.getParameter("selectedItem").getBindingContext().getProperty("PostCode"));
				oModel.setProperty("/Ort", oEvent.getParameter("selectedItem").getBindingContext().getProperty("City"));
				oModel.setProperty("/GPNum", oEvent.getParameter("selectedItem").getBindingContext().getProperty("BuPartner"));
				oModel.setProperty("/VKNum", oEvent.getParameter("selectedItem").getBindingContext().getProperty("BuagID"));
			}
		},
		handleReset: function (oEvent) {
			this.getView().byId("CustomerName").setValue("");
			this.getView().byId("Street").setValue("");
			this.getView().byId("HouseNum").setValue("");
			this.getView().byId("PostCode").setValue("");
			this.getView().byId("City").setValue("");
			this.getView().byId("BuPartner").setValue("");
			this.getView().byId("BuagID").setValue("");
		},
		/**
		 * Merges the odata to the inputmodel
		 * @function
		 * @param {Object} oObject odata Lead.
		 * @private
		 */
		mergeInputModel: function (oObject) {
			// this.getView().getModel("input").setProperty()
			// this.getView().getModel("input")
			if (oObject !== undefined && oObject.Guid !== undefined ) {
				var oModel = this.getView().getModel("input");

				oModel.setProperty("LeadChannel", oObject.LeadChannel);
				oModel.setProperty("LeadType", oObject.LeadType);
				oModel.setProperty("Guid", oObject.Guid);
				oModel.setProperty("ChangedAt", oObject.ChangedAt);
				oModel.setProperty("ChangedBy", oObject.ChangedBy);
				oModel.setProperty("CreatedAt", oObject.CreatedAt);
				oModel.setProperty("CreatedBy", oObject.CreatedBy);
				oModel.setProperty("Partner", oObject.Partner);
				oModel.setProperty("Rating", oObject.Rating);
				oModel.setProperty("Score", oObject.Score);
				oModel.setProperty("ScoreMax", oObject.ScoreMax);
				oModel.setProperty("Status", oObject.Status);

				var jsonString = oObject.LeadXml;
				if (jsonString !== "") {
					var oData = JSON.parse(jsonString);
					oData.Datum = new Date(oData.Datum);
					oModel.setData(oData);
				};

			}

		}

	});

});