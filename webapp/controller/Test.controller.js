sap.ui.define([
	'eon/app/com/survey/controller/BaseController',
	'jquery.sap.global',
	'sap/ui/core/mvc/Controller',
	'sap/m/UploadCollectionParameter',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/viz/ui5/format/ChartFormatter",
	"sap/viz/ui5/api/env/Format"
], function(BaseController, jQuery, Controller, UploadCollectionParameter, JSONModel, MessageToast, MessageBox, ChartFormatter, Format) {
	"use strict";

	var WizardController = BaseController.extend("eon.app.com.survey.controller.ViewPV", {

		onInit: function() {

			this._wizard = this.getView().byId("CreateProductWizard");
			this._oNavContainer = this.getView().byId("wizardNavContainer");
			this._oWizardContentPage = this.getView().byId("wizardContentPage");
			//var sPath = jQuery.sap.getModulePath("eon.app.com.survey.model", "/InputPV.json");
			this.model = new sap.ui.model.json.JSONModel({
				AbwRgJa: false,
				AbwRgNein: false,
				AbwEigJa:false,
				AbwEigNein:false,
				ErzeugJa:false,
				ErzeugNein:false,
				StatikerJa:false,
				StatikerNein: false,
				NetzanfrageJa: false,
				NetzanfrageNein: false,
				AnschlusszusageJa:false,
				AnschlusszusageNein:false
			});
			this.getView().setModel(this.model, "input");

			var viewModel = new JSONModel({
				"pics": jQuery.sap.getModulePath("eon.app.com.survey.media", ""),
				"busy": "false"
			});
			this.getView().setModel(viewModel, "viewModel");

			this.getRouter().getRoute("pv").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);

			// try to load sap.suite.ui.commons for using ChartContainer
			// sap.suite.ui.commons is available in sapui5-sdk-dist but not in demokit
			var bSuiteAvailable = jQuery.sap.sjax({
				type: "HEAD",
				url: sap.ui.resource("sap.suite.ui.commons", "library.js")
			}).success;

			if (bSuiteAvailable) {
				sap.ui.getCore().loadLibrary("sap.suite.ui.commons");
				var vizframe = this.getView().byId("idVizFrame");
				var oChartContainerContent = new sap.suite.ui.commons.ChartContainerContent({
					icon: "sap-icon://horizontal-bar-chart",
					title: "Lastgang",
					content: [vizframe]
				});
				var oChartContainer = new sap.suite.ui.commons.ChartContainer({
					content: [oChartContainerContent]
				});
				oChartContainer.setShowFullScreen(true);
				oChartContainer.setAutoAdjustHeight(true);
				this.getView().byId('chartFixFlex').setFlexContent(oChartContainer);
			}

			Format.numericFormatter(ChartFormatter.getInstance());
			var formatPattern = ChartFormatter.DefaultPattern;
			var oVizFrame = this.oVizFrame = this.getView().byId("idVizFrame");

			var oPopOver = this.getView().byId("idPopOver");
			oPopOver.connect(oVizFrame.getVizUid());
			//oPopOver.setFormatString({"Datum":"PROF_VALUE","Cost":"PROF_DATE"});
			oPopOver.setFormatString({
				"Datum": "PROF_VALUE",
				"PROF_DATE": "kWh"
			});
			/*  oPopOver.setCustomDataControl( function(data){
			  return new sap.m.Label("testID", {text:'Test'});
			  } );*/
			/*var sPath = jQuery.sap.getModulePath("eon.app.com.survey.model", "/edm.json");
			var dataModel = new JSONModel(sPath);*/
			/*	oVizFrame.setVizProperties({
					plotArea: {
						dataLabel: {
							formatString: formatPattern.SHORTFLOAT_MFD2,
							visible: false
						},
						marker: {
							visible: false
						},
						window: {
							start: "firstDataPoint",
							end: "lastDataPoint"
						}

					},
					valueAxis: {
						label: {
							//formatString: formatPattern.SHORTFLOAT
						},
						title: {
							visible: false
						}
					},
					timeAxis: {
						interval: {
							unit: "auto"
						},
						title: {
							visible: false
						}
					},
					title: {
						visible: true,
						text: 'Aktueller Lastgang'
					}
				});*/
			// Todo Navigationsstuff onObhectMathecd	

			var oLastGangModel = new JSONModel({});
			this.setModel(oLastGangModel, "lastgang");

			this.getOwnerComponent().getModel().read("/ProfileValuesSet", {
				success: $.proxy(function(data) {
					var json = JSON.parse(data.results[0].ProfValues);
					json.VALUES.map(function(x) {
						//ISO to Short date
						//var d = new Date(x.PROF_DATE);
						var arr = x.PROF_DATE.split("-");
						x.PROF_DATE = arr[1] + "/" + arr[2] + "/" + arr[0];
						return x;

					});

					this.getModel("lastgang").setProperty("/", json);
				}, this)
			});
		},
		onExit: function() {
			if (this._oDialog) {
				this._oDialog.destroy();
			}
		},
		onAfterRendering: function() {
			var name = this.getView().byId("CustomerName").getValue();
			if (name.length === 0) {
				this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Warning);
			} else {
				this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Warning);
			}
			this.getView().byId("Datum").setDateValue(new Date());
			// Get the wizard and its progress navigator
			/*			var oWizard = this.getView().byId("CreateProductWizard");
						var oWizardProgressNavigator = this.getView().byId("CreateProductWizard-progressNavigator");
			*/
			// First step needs to be included, or else this hack does not work and the user ends up at step 2
			/*			for (var i = 1, iLength = oWizard.getSteps().length; i <= iLength; i++) {
							oWizardProgressNavigator._moveToStep(i);
						}
						this._wizard.validateStep(this.getView().byId("RegionStep"));
						this._wizard.validateStep(this.getView().byId("ExpertStep"));
						this._wizard.validateStep(this.getView().byId("SalesStep"));
						this._wizard.validateStep(this.getView().byId("PMOStep"));
						this._wizard.validateStep(this.getView().byId("LeadQStep"));*/
			// Go to the first step
			// oWizardProgressNavigator._moveToStep(1);

		},
		save: function() {
			this.saveLead(this.getModel("input"));
		},
		calculateScore: function() {
			var addScore = 0;

			var category = this.getModel("input").getProperty("/GroesseAnlage");
			var score;

			if (category <= 49) {
				score = 0;
			} else if (category >= 50 && category <= 94) {
				score = 10;
			} else if (category >= 95 && category <= 750) {
				score = 20;
			}

			this.getModel("input").setProperty("/GroesseAnlageScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/DachFrei");

			if (category <= 399) {
				score = 0;
			} else if (category >= 400 && category <= 799) {
				score = 10;
			} else if (category >= 800) {
				score = 20;
			}
			this.getModel("input").setProperty("/DachFreiScore", score);

			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/NutzDauer");

			if (category <= 750) {
				score = 0;
			} else if (category >= 751 && category <= 1500) {
				score = 2.5;
			} else if (category >= 1501 && category <= 2250) {
				score = 5;
			} else if (category >= 2251 && category <= 3000) {
				score = 10;
			} else if (category >= 3001 && category <= 3750) {
				score = 15;
			} else if (category >= 3751) {
				score = 20;
			}
			this.getModel("input").setProperty("/NutzDauerScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/TagVerbrauch");

			if (category <= 30000) {
				score = 0;
			} else if (category >= 30001 && category <= 100000) {
				score = 10;
			} else if (category >= 100001 && category <= 500000) {
				score = 15;
			} else if (category >= 500001) {
				score = 20;
			}
			this.getModel("input").setProperty("/TagVerbrauchScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/Rendite");

			if (category <= 6) {
				score = 0;
			} else if (category >= 7 && category <= 8.4) {
				score = 10;
			} else if (category >= 8.5 && category <= 9.9) {
				score = 15;
			} else if (category >= 10) {
				score = 20;
			}
			this.getModel("input").setProperty("/RenditeScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/StatDach");

			if (category === "nicht geprueft") {
				score = 0;
			} else if (category === "geprueft") {
				score = 20;
			}
			this.getModel("input").setProperty("/StatDachScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/AusrSonne");

			if (category === "Nord") {
				score = 0;
			} else if (category === "OstSuedWest") {
				score = 10;
			}
			this.getModel("input").setProperty("/AusrSonneScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/EigentGeb");

			if (category === "Nein") {
				score = 0;
			} else if (category === "Ja") {
				score = 20;
			}
			this.getModel("input").setProperty("/EigentGebScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Stadtlage");

			if (category === "Nein") {
				score = 10;
			} else if (category === "Ja") {
				score = 0;
			}
			this.getModel("input").setProperty("/StadtlageScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/EEGFrei");

			if (category === "Ja") {
				score = 0;
			} else if (category === "Nein") {
				score = 20;
			}
			this.getModel("input").setProperty("/EEGFreiScore", score);
			addScore = addScore + score;
			score = 0;
			category = this.getModel("input").getProperty("/PVvorhand");

			if (category === "Ja") {
				score = 0;
			} else if (category === "Nein") {
				score = 10;
			}
			this.getModel("input").setProperty("/PVvorhandScore", score);
			addScore = addScore + score;
			score = 0;
			parseInt(this.getModel("input").setProperty("/SumScoreQC", addScore));
		},

		calculateScoreSuccess: function() {
			var addScore = 0;

			var category = this.getModel("input").getProperty("/GesprPartner");
			var score;

			if (category === "Entscheider") {
				score = 20;
			} else if (category === "andere Person") {
				score = 0;
			}
			this.getModel("input").setProperty("/GesprPartnerScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/GeschBez");

			if (category === "neutral") {
				score = 0;
			} else if (category === "sehr gut") {
				score = 15;
			}
			this.getModel("input").setProperty("/GeschBezScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Interest");

			if (category === "Ansprache") {
				score = 0;
			} else if (category === "Eigeninitiative") {
				score = 20;
			}
			this.getModel("input").setProperty("/InterestScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Competition");

			if (category === "andere Wettbewerber") {
				score = 10;
			} else if (category === "kein Wettbewerber") {
				score = 20;
			}
			this.getModel("input").setProperty("/CompetitionScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Decision");

			if (category === "definiert") {
				score = 15;
			} else if (category === "nicht definiert") {
				score = 0;
			}
			this.getModel("input").setProperty("/DecisionScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Intention");

			if (category === "Ja") {
				score = 10;
			} else if (category === "Nein") {
				score = 0;
			}
			this.getModel("input").setProperty("/IntentionScore", score);
			addScore = addScore + score;
			score = 0;

			category = this.getModel("input").getProperty("/Budget");

			if (category === "Ja") {
				score = 20;
			} else if (category === "Nein") {
				score = 0;
			}
			this.getModel("input").setProperty("/BudgetScore", score);
			addScore = addScore + score;
			score = 0;

			parseInt(this.getModel("input").setProperty("/SumScoreSuccess", addScore));
		},

		additionalInfoValidation: function() {
			var name = this.getView().byId("CustomerName").getValue();

			if (name.length === 0) {
				this._wizard.invalidateStep(this.getView().byId("RegionStep"));
				this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Error);
			} else {
				this._wizard.validateStep(this.getView().byId("RegionStep"));
				this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Success);
			}
		},

		_handleNavigationToStep: function(iStepNumber) {
			var that = this;

			function fnAfterNavigate() {
				that._wizard.goToStep(that._wizard.getSteps()[iStepNumber]);
				that._oNavContainer.detachAfterNavigate(fnAfterNavigate);
			}

			this._oNavContainer.attachAfterNavigate(fnAfterNavigate);
			this.backToWizardContent();
		},
		_handleMessageBoxOpen: function(sMessage, sMessageBoxType) {
			var that = this;
			MessageBox[sMessageBoxType](sMessage, {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						that._handleNavigationToStep(0);
						that._wizard.discardProgress(that._wizard.getSteps()[0]);
					}
				}
			});
		},
		_setEmptyValue: function(sPath) {
			this.model.setProperty(sPath, "n/a");
		},
		handleWizardCancel: function() {
			this._handleMessageBoxOpen("Wollen Sie wirklich die Leaderfassung abbrechen?", "warning");
		},
		handleWizardSubmit: function() {
			MessageToast.show("Funktion noch nicht implementiert!");
		},

		discardProgress: function() {
			this._wizard.discardProgress(this.getView().byId("CustomerDataStep"));

			var clearContent = function(content) {
				for (var i = 0; i < content.length; i++) {
					if (content[i].setValue) {
						content[i].setValue("");
					}

					if (content[i].getContent) {
						clearContent(content[i].getContent());
					}
				}
			};

			clearContent(this._wizard.getSteps());
		},
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			var sGuid = oEvent.getParameter("arguments").Guid;
			if (sGuid !== undefined) {
				this.getModel().metadataLoaded().then(function() {
					var sObjectPath = this.getModel().createKey("LeadSet", {
						Guid: sGuid
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
			}
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function(sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("viewModel");

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);

			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function() {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				//	this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				//this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
		}

	});

	return WizardController;
});