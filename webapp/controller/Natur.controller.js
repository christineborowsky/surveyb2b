sap.ui.define([
	"eon/app/com/survey/controller/BaseController",
	"jquery.sap.global",
	"sap/ui/core/mvc/Controller",
	"sap/m/UploadCollectionParameter",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/model/ValidateException",
	"sap/ui/model/SimpleType",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/List",
	"sap/m/StandardListItem",
	"sap/ui/core/Fragment",
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV',
	"sap/ui/core/routing/History"
], function (BaseController, jQuery, Controller, UploadCollectionParameter, JSONModel, MessageToast, MessageBox, ValidateException,
	SimpleType, Filter, FilterOperator, Button, Dialog, List, StandardListItem, Fragment, Export, ExportTypeCSV, History) {
	"use strict";
	var WizardController = BaseController.extend("eon.app.com.survey.controller.Natur", {
		onInit: function () {

			this._wizard = this.getView().byId("CreateProductWizard");
			this._oNavContainer = this.getView().byId("wizardNavContainer");
			this._oWizardContentPage = this.getView().byId("wizardContentPage");
			var jsonModel = new sap.ui.model.json.JSONModel({
				LeadType: "NATUR",
				LeadChannel: "MOB",
				AnschlussGasJa: false,
				AnschlussGasNein: false,
				VersorgungsgebietJa: false,
				VersorgungsgebietNein: false,
				WarmwasserJa: false,
				WarmwasserNein: false,
				PufferJa: false,
				PufferNein: false,
				BHKWGebaeudeJa: false,
				BHKWGebaeudeNein: false,
				BHKWIndustrieJa: false,
				BHKWIndustrieNein: false,
				EinsatzStromJa: false,
				EinsatzStromNein: false,
				EinsatzGasJa: false,
				EinsatzGasNein: false,
				EinsatzFlGasJa: false,
				EinsatzFlGasNein: false,
				EinsatzOelJa: false,
				EinsatzOelNein: false,
				EinsatzBioJa: false,
				EinsatzBioNein: false,
				WaermekundeJa: false,
				WaermekundeNein: false
			});

			this.getView().setModel(jsonModel, "input");
			var viewModel = new JSONModel({
				"pics": jQuery.sap.getModulePath("eon.app.com.survey.media", ""),
				"busy": "false"
			});
			this.getView().setModel(viewModel, "viewModel");
			this.getRouter().getRoute("natur").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().getRoute("naturCreate").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);
		},
		onAfterRendering: function () {
			//var name = sap.ui.core.Fragment.byId("custFragment", "CustomerName");
			/*	var name = this.getView().byId("CustomerName").getValue();
				if (name.length === 0) {
					this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Warning);
				} else {
					this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Success);
				}
				this.getView().byId("Datum").setDateValue(new Date());*/
			/*	if( this.getModel("input").getProperty("Datum")===""){
					this.getModel("input").getProperty("Datum", new Date() );
					
				}*/
		},
		onDataExport: function (oEvent) {

			var oModel = this.getModel("input");

			var oExport = new Export({

				// Type that will be used to generate the content. Own ExportType's can be created to support other formats
				exportType: new ExportTypeCSV({
					separatorChar: ";"
				}),

				// Pass in the model created above
				models: oModel,

				// binding information for the rows aggregation
				rows: {
					path: "/VALUES"
				},

				// column definitions with column name and binding info for the content

				columns: [{
					name: "Datum",
					template: {
						content: {
							parts: ["PROF_DATE"],
							formatter: function (value) {
								if (value) {
									return "    " + value.toLocaleDateString();
								} else {
									return "";
								}
							}
						}
					}
				}, {
					name: "Zeit",
					template: {
						content: "{PROF_TIME}"
					}
				}, {
					name: "KW",
					template: {
						content: "{PROF_VALUE}"
					}
				}]
			});

			// download exported file

			oExport.saveFile("Verbrauchswerte_" + this.getModel("objectView").getProperty("/dateFrom").toLocaleDateString() + "_" + this.getModel(
				"objectView").getProperty("/dateTo").toLocaleDateString()).catch(function (oError) {
				MessageBox.error("Error when downloading data. Browser might not be supported!\n\n" + oError);
			}).then(function () {
				oExport.destroy();
			});
		},
		newLead: function () {
			this.getModel("input").destroy();
		},
		save: function () {
			this.saveLead(this.getModel("input"));
		},
		cancel: function () {
			this._handleMessageBoxOpen("Wollen Sie wirklich die Leaderfassung abbrechen?", "warning");
		},

		setProductType: function (evt) {
			var productType = evt.getSource().getTitle();
			this.model.setProperty("/productType", productType);
			this.getView().byId("ProductStepChosenType").setText("Chosen product type: " + productType);
			this._wizard.validateStep(this.getView().byId("CustomerDataStep"));
		},
		setProductTypeFromSegmented: function (evt) {
			var productType = evt.getParameters().button.getText();
			this.model.setProperty("/productType", productType);
			this._wizard.validateStep(this.getView().byId("CustomerDataStep"));
		},
		onSearch: function (oEvent) {},
		typeSearchString: SimpleType.extend("email", {
			formatValue: function (oValue) {
				return oValue;
			},
			parseValue: function (oValue) {
				//parsing step takes place before validating step, value can be altered
				return oValue;
			},
			validateValue: function (oValue) {
				if (oValue.length === 0) {
					return;
				} else if (oValue.length < 3) {
					throw new ValidateException("'" + oValue + "' ist kein g\xFCltiger String");
				}
			}
		}),
		additionalInfoValidation: function (oEvent) {
			var name = oEvent.getParameter("newValue");
			/*	//var name = sap.ui.core.Fragment.byId(this.createId("custFragment"), "testlabel");
				if (name.length === 0) {
					this._wizard.invalidateStep(this.getView().byId("CustomerDataStep"));
					this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Error);
				} else {
					this._wizard.validateStep(this.getView().byId("CustomerDataStep"));
					this.getView().byId("CustomerName").setValueState(sap.ui.core.ValueState.Success);
				}
					if (name.length > 4) {
						var oFilters = [
					new sap.ui.model.Filter("NameOrg1", "EQ", name)
					///new sap.ui.model.Filter("Street", "EQ", this.getView().byId("Street").getValue())
				];
						this.filter = oFilters;
				var oModel = this.getModel();
					oModel.read("/DubSearchSet", {
						filters: oFilters,
						success: $.proxy(function (oRetrievedResult) {
							if (oRetrievedResult.results.length > 0) {
								this.handleSelectDialogPress();
							}
						}, this),
						error: function (oError) {
							MessageToast.show("Datenbank nicht erreicht");
						}
					});
				}
				return;*/

			if (name.length === 10) {
				var oFilters = [
					new sap.ui.model.Filter("BuPartner", "EQ", name)
					///new sap.ui.model.Filter("Street", "EQ", this.getView().byId("Street").getValue())
				];
				this.filter = oFilters;
				var oModel = this.getModel();
				oModel.read("/DubSearchSet", {
					filters: oFilters,
					success: $.proxy(function (oRetrievedResult) {
						if (oRetrievedResult.results.length === 1) {
							this.getModel("viewModel").setProperty("/gpName", oRetrievedResult.results[0].Fullname);
							//Brings up select Popup: this.handleSelectDialogPress();
						}else{
								MessageToast.show("Geschäfstpartner "+ name +" nicht gefunden" );
						}
					}, this),
					error: function (oError) {
						MessageToast.show("Datenbank nicht erreicht");
					}
				});
			}
		},
		onExit: function () {
			if (this._oDialog) {
				this._oDialog.destroy();
			}
		},
		createAccount: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("AccountCreate", true);

		},
		/*	handleSelectDialogPress: function(oEvent) {
				if (!this._oDialog) {
					this._oDialog = sap.ui.xmlfragment("eon.app.com.survey.fragments.Dialog", this);
					this._oDialog.setModel(this.getView().getModel());
				}
				
				if( oModel.getProperty("/Kundenname")!== null){
					
				}
				
				var oFilters = [
					new sap.ui.model.Filter("NameOrg1", "EQ", this.getView().byId("CustomerName").getValue()),
					new sap.ui.model.Filter("PostCode", "EQ", this.getView().byId("PostCode").getValue()),
					new sap.ui.model.Filter("Street", "EQ", this.getView().byId("Street").getValue()),
					new sap.ui.model.Filter("HouseNum", "EQ", this.getView().byId("HouseNum").getValue()),
					new sap.ui.model.Filter("City", "EQ", this.getView().byId("City").getValue()),
					new sap.ui.model.Filter("BuPartner", "EQ", this.getView().byId("BuPartner").getValue()),
					new sap.ui.model.Filter("BuagID", "EQ", this.getView().byId("BuagID").getValue())
				];
				this._oDialog.getBinding("items").filter(oFilters);
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
				this._oDialog.open();
			},*/
		/*		handleSearch: function(oEvent) {
					var sValue = oEvent.getParameter("value");
					var oFilter = new Filter("Name", sap.ui.model.FilterOperator.Contains, sValue);
					var oBinding = oEvent.getSource().getBinding("items");
					oBinding.filter([oFilter]);
				},*/

		_handleNavigationToStep: function (iStepNumber) {

			function fnAfterNavigate() {
				this._wizard.goToStep(this._wizard.getSteps()[iStepNumber]);
				this._oNavContainer.detachAfterNavigate(fnAfterNavigate);
			}
			this._oNavContainer.attachAfterNavigate(fnAfterNavigate);
			//this.backToWizardContent();
		},
		_handleMessageBoxOpen: function (sMessage, sMessageBoxType) {

			MessageBox[sMessageBoxType](sMessage, {
				actions: [
					MessageBox.Action.YES,
					MessageBox.Action.NO
				],
				onClose: $.proxy(function (oAction) {
					if (oAction === MessageBox.Action.YES) {
						this._handleNavigationToStep(0);
						this._wizard.discardProgress(this._wizard.getSteps()[0]);

						var oHistory = History.getInstance();
						var sPreviousHash = oHistory.getPreviousHash();

						if (sPreviousHash !== undefined) {
							window.history.go(-1);
						} else {
							var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
							oRouter.navTo("overview", true);
						}

					}
				}, this)
			});
		},
		_setEmptyValue: function (sPath) {
			this.model.setProperty(sPath, "n/a");
		},
		handleWizardCancel: function () {
			this._handleMessageBoxOpen("Wollen Sie wirklich die Leaderfassung abbrechen?", "warning");
		},
		productWeighStateFormatter: function (val) {
			return isNaN(val) ? "Error" : "None";
		},
		discardProgress: function () {
			this._wizard.discardProgress(this.getView().byId("CustomerDataStep"));
			var clearContent = function (content) {
				for (var i = 0; i < content.length; i++) {
					if (content[i].setValue) {
						content[i].setValue("");
					}
					if (content[i].getContent) {
						clearContent(content[i].getContent());
					}
				}
			};
			this.model.setProperty("/productWeightState", "Error");
			this.model.setProperty("/productNameState", "Error");
			clearContent(this._wizard.getSteps());
		},
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function (oEvent) {
			var sGuid = oEvent.getParameter("arguments").Guid;
			if (sGuid !== undefined) {
				this.getModel().metadataLoaded().then(function () {
					var sObjectPath = this.getModel().createKey("LeadSet", {
						Guid: sGuid
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
			} else {
				this.getModel().metadataLoaded().then(function () {
					var sObjectPath = this.getModel().createEntry("LeadSet");
					this._bindView(sObjectPath.sPath);
				}.bind(this));
			}
		},
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function (sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("viewModel");

			this.mergeInputModel(this.getModel().getProperty(sObjectPath));

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);
			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function () {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},
		_onBindingChange: function () {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				//	this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				//this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
			var sPath = oElementBinding.getPath();
			var oResourceBundle = this.getResourceBundle();
			var oObject = oView.getModel().getObject(sPath);

			//	var sGuid = oObject.Guid;
			//	var oViewModel = this.getModel("viewModel");

			/*	oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("shareSaveTileAppTitle", ["Lead"]));
				oViewModel.setProperty("/shareOnJamTitle", sGuid);
				oViewModel.setProperty("/shareSendEmailSubject", oResourceBundle.getText("shareSendEmailObjectSubject", [sGuid]));
				oViewModel.setProperty("/shareSendEmailMessage", oResourceBundle.getText("shareSendEmailObjectMessage", [
					sGuid,
					location.href
				]));*/

		}

	});
	return WizardController;
});