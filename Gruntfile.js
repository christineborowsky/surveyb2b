module.exports = function(grunt) {
	"use strict";
	grunt.loadNpmTasks("@sap/grunt-sapui5-bestpractice-build");

	grunt.config.merge({
		clean: {
			final: {
				src: ["localService/", "dist/localService", "dist/localService/", "/dist/localService", "/dist/localService/"],
				filter: function(a, b) {
					return true;
					grunt.log.writeln(a, b);
				}
			}
		},
		copy: {
			preload: {
				expand: true,
				src: "Component-preload.js",
				dest: "<%= dir.dist %>",
				cwd: "<%= dir.tmpDir %>"
			},
			copyDbgToDist: {
				files: [{
					expand: true,
					src: "['**/*.js', '!localService']",
					dest: "<%= dir.disxt %>",
					cwd: "<%= dir.tmpDirDbg2 %>",
					filter: function() {
						// prevent js from localService to be copied
						return false;
					},
					rename: function(dest, src) {
						return dest + "/" + src.replace(/((\.view|\.fragment|\.controller)?\.js)/, "-dbg$1");
					}
				}, {
					expand: true,
					src: "**/*.css",
					dest: "<%= dir.dixst %>",
					cwd: "<%= dir.tmpDirDbg3 %>",
					rename: function(dest, src) {
						return dest + "/" + src.replace(".css", "-dbg.css");
					},
					filter: function() {
						// prevent js from localService to be copied
						return false;
					}
				}]
			}
		},
		uglify: {
			all: {
				files: [{
					expand: true,
					src: "**/*.js",
					dest: "<%= dir.dist %>",
					cwd: "<%= dir.webapp %>",
					filter: function(filepath) {
						// prevent js from localService to be copied
						return !filepath.match(new RegExp("<%= dir.webapp %>" + "(\\/|\\\\)localService", "gi"));
					}
				}]
			}
		}

	});

	grunt.registerTask("default", [
		"clean",
		"lint",
		"build",
		"uglify:all",
		"copy:copyTmpToDist",
		"clean:final"
	]);

};